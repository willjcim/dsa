#ifndef DOUBLY_LINKED_LIST
#define DOUBLY_LINKED_LIST

#include <iostream>

using namespace std;

template <typename T>
class DoublyLinkedList
{
    template <typename R>
    struct Node
    {
        R       mData;
        Node<R>* mPrevious, * mNext;

        /*      Pre:  None
         *     Post:  This object is initialized using default values
         *  Purpose:  To initialize date object
         ********************************************************************/
        Node()
        {
            mData = R();
            mPrevious = NULL;
            mNext = NULL;
        }


        /*      Pre:  None
         *     Post:  This object is initialized using specified data
         *  Purpose:  To intialize date object
         ********************************************************************/
        Node(R data)
        {
            mData = data;
            mPrevious = NULL;
            mNext = NULL;
        }
    };


private:
    Node<T>* mHead, * mTail;
    int     mCount;

public:
    DoublyLinkedList();
    DoublyLinkedList(const DoublyLinkedList<T>& list);
    ~DoublyLinkedList();

    int  getCount();
    T    getData(int index);
    void setData(int index, T data);

    bool binarySearch(T searchKey);
    void clear();
    void display();
    void displayInReverse();
    bool insert(T data);
    bool isEmpty();
    bool isExist(T searchKey);
    bool remove(T searchKey);
    T    removeAt(int index);

    T operator[](int index);
};


/*      Pre:  None
 *     Post:  This object is initialized using the default
 *  Purpose:  To initialize date object
 ************************************************************************/
template <typename T>
DoublyLinkedList<T>::DoublyLinkedList()
{
    mHead = NULL;
    mTail = NULL;
    mCount = 0;
}


/*      Pre:  None
 *     Post:  The object is instantiated using the information from
 *            the parameter
 *  Purpose:  This is the copy constructor for the class
 ************************************************************************/
template <typename T>
DoublyLinkedList<T>::DoublyLinkedList(const DoublyLinkedList<T>& list)
{
    Node<T>* tmp;

    tmp = list.mHead;
    while (tmp != NULL)
    {
        insert(tmp->mData);
        tmp = tmp->mNext;
    }
}


/*      Pre:  None
 *     Post:  All the nodes in the list is deleted
 *  Purpose:  To remove all the nodes in the list
 ************************************************************************/
template <typename T>
DoublyLinkedList<T>::~DoublyLinkedList()
{
    clear();
}


/*      Pre:  The object is instantiated
 *     Post:  The number of nodes in the linked list is returned to
 *            the caller
 *  Purpose:  To retrieve the number of nodes in the list
 ************************************************************************/
template <typename T>
int DoublyLinkedList<T>::getCount()
{
    return mCount;
}


/*      Pre:  The list is instantiated and the index is valid
 *     Post:  The data in the specified index is returned to the caller
 *  Purpose:  To retrieve the specified nodes in the list
 ************************************************************************/
template <typename T>
T DoublyLinkedList<T>::getData(int index)
{
    Node<T>* tmp;
    T data = T();
    int i;

    if (index >= 0 && index < mCount)
    {
        i = 0;
        tmp = mHead;
        while (i < index)
        {
            i++;
            tmp = tmp->mNext;
        }

        data = tmp->mData;
    }

    return data;
}


/*      Pre:  The list is instantiated, the index is valid and the
 *            data is available
 *     Post:  The data in the specified index is updated with the
 *            specified data
 *  Purpose:  To update the specified nodes in the list
 ************************************************************************/
template <typename T>
void DoublyLinkedList<T>::setData(int index, T data)
{
    removeAt(index);
    insert(data);
}


/*      Pre:  The list is initiated
 *     Post:  True is the search key exists in the list,
 *            otherwise return false
 *  Purpose:  To simulate a binary search within doubly
 *            linked list
 ************************************************************************/
template <typename T>
bool DoublyLinkedList<T>::binarySearch(T searchKey)
{
    // Declares nodes
    Node<T>* middleBound = mHead;
    Node<T>* lowerBound = mHead;
    Node<T>* upperBound = mTail;
    Node<T>* oneBefore, * tmp;

    // Initiates the doubly linked list
    tmp = mHead->mNext;
    oneBefore = mHead;
    while (tmp != NULL) {
        tmp->mPrevious = oneBefore;
        tmp = tmp->mNext;
        oneBefore = oneBefore->mNext;
    }

    // Initializes variables
    int lowerIndex = 0;
    int upperIndex = mCount;
    int middleIndex;

    while (lowerBound->mData <= upperBound->mData) {
        int i = 0;
        middleIndex = (lowerIndex + upperIndex) / 2;
        middleBound = mHead;
        do {
            middleBound = middleBound->mNext;
            i++;
        } while (i != middleIndex + 1); // Sets middleBound to the correct index

        if (middleIndex == mCount - 1) { // If middle index goes above the total count of variables, return false
            return false;
        }

        if (middleBound->mData == searchKey) { // If the searchkey is found, return true
            return true;
        }
        else if (middleBound->mData > searchKey) { // If the search key is below the middle bound, subtract one from upper index, and set the upper bound to middleBound->mPrevious
            upperBound = middleBound->mPrevious;
            upperIndex = middleIndex - 1;
        }
        else {
            lowerBound = middleBound->mNext; // If the search key is above the middle bound, add one to the lower index, and set the lower bound to middleBound->mNext
            lowerIndex = middleIndex + 1;
        }
    }
    delete middleBound, upperBound, lowerBound, tmp, oneBefore;
    return false;
}


/*      Pre:  The list is initiated
 *     Post:  All the nodes in the list is deleted
 *  Purpose:  To remove all the nodes in the list
 ************************************************************************/
template <typename T>
void DoublyLinkedList<T>::clear()
{
    while (mHead != NULL)
        removeAt(0);
}



/*      Pre:  The list is instantiated
 *     Post:  The entire list is displayed on the screen
 *  Purpose:  To show the content of the list
 ************************************************************************/
template <typename T>
void DoublyLinkedList<T>::display()
{
    Node<T>* tmp;

    if (mHead == NULL)
    {
        cout << "The list is empty\n";
        return;
    }

    tmp = mHead;
    while (tmp != NULL)
    {
        cout << tmp->mData << " ";
        tmp = tmp->mNext;
    }
    cout << endl;
}


/*      Pre:  The list is instantiated
 *     Post:  The entire list is displayed on the screen
 *            in reverse order
 *  Purpose:  To show the content of the list from tail
 *            back to head of the list
 ************************************************************************/
template <typename T>
void DoublyLinkedList<T>::displayInReverse()
{
    // Declares nodes
    Node<T>* tmp, * oneBefore;

    // Checks if the list is empy
    if (mHead == NULL) {
        cout << "The list is empty" << endl;
    }

    // Initiates the doubly linked list
    tmp = mHead->mNext;
    oneBefore = mHead;
    while (tmp != NULL) {
        tmp->mPrevious = oneBefore;
        tmp = tmp->mNext;
        oneBefore = oneBefore->mNext;
    }

    tmp = mTail; // Start at the end of the list
    while (tmp != mHead) { // On each iteration of the loop, output the data, and set tmp to tmp->mPrevious, loop will end when it reaches the head
        cout << tmp->mData << " ";
        tmp = tmp->mPrevious;
    }
    cout << endl;
}


/*      Pre:  The list is instantiated and the data is available
 *     Post:  The data is inserted in ascending order
 *  Purpose:  To insert a data into the list in ascending order.
 *            However, if the data already existed in the list, it will
 *            not be added again
 ************************************************************************/
template <typename T>
bool DoublyLinkedList<T>::insert(T data)
{
    Node<T>* tmp, * oneBefore, * newNode;

    newNode = new Node<T>(data);
    if (newNode == NULL)
        return false;

    if (mHead == NULL)
    {
        mHead = newNode;
        mTail = newNode;
    }
    else
    {
        mTail->mNext = newNode;
        mTail = newNode;
    }

    mCount++;

    return true;
}


/*      Pre:  The list is instantiated
 *     Post:  The function returns true is the list is empty; false
 *            otherwise
 *  Purpose:  To determine if the list is empty
 ************************************************************************/
template <typename T>
bool DoublyLinkedList<T>::isEmpty()
{
    return mHead == NULL;
}


/*      Pre:  The list is instantiated and the searchKey is available
 *     Post:  The function returns true if the search key exists in the
 *            list; otherwise false
 *  Purpose:  To determine if a specific value exists in the list or not
 ************************************************************************/
template <typename T>
bool DoublyLinkedList<T>::isExist(T searchKey)
{
    bool found = false;
    Node<T>* tmp;

    tmp = mHead;

    while (tmp != NULL)
    {
        if (tmp->mData == searchKey)
        {
            found = true;
            break;
        }
        else if (tmp->mData > searchKey)
            break;

        tmp = tmp->mNext;
    }
    return found;
}


/*      Pre:  The list is instantiated and the searchKey is available
 *     Post:  If the searchKey exists, removes it from the list and the
 *            function returns true; otherwise the function does nothing
 *            and returns false
 *  Purpose:  To remove a specific value from the list
 ************************************************************************/
template <typename T>
bool DoublyLinkedList<T>::remove(T searchKey)
{
    bool deleted = false;
    Node<T>* tmp, * oneBefore;

    // case 1: empty list
    if (mHead != NULL)
    {
        // case 2: deleting at the head
        if (mHead->mData == searchKey)
        {
            mHead = mHead->mNext;
            mHead->mPrevious = NULL;
            //tmp = mHead;
            //tmp->mNext = NULL;

            deleted = true;
            //delete tmp;

            if (mHead == NULL)
                mTail = NULL;
        }
        else
        {
            tmp = mHead;
            oneBefore = mHead;

            while (tmp != NULL)
            {
                if (tmp->mData == searchKey)
                    break;

                oneBefore = tmp;
                tmp = tmp->mNext;
            }

            if (tmp != NULL)
            {
                // case 3: deleting at the tail
                if (tmp == mTail)
                {
                    mTail = oneBefore;
                    mTail->mNext = NULL;
                }

                // case 4: deleting at the middle
                else
                {
                    oneBefore->mNext = tmp->mNext;
                    tmp->mNext = NULL;
                }

                deleted = true;
                delete tmp;
            }
        }
    }

    if (deleted)
        mCount--;

    return deleted;
}


/*      Pre:  The list is instantiated and the index is valid
 *     Post:  Remove the element in the specified index location and
 *            returns its content to the caller.  If the index location
 *            is invalid, the function returns the default value
 *  Purpose:  To remove an item in the specified index location
 ************************************************************************/
template <typename T>
T DoublyLinkedList<T>::removeAt(int index)
{
    Node<T>* tmp, * oneBefore;
    T       data = T();
    int     i;

    if (index < 0 || index >= mCount);
    else if (mHead != NULL)
    {
        tmp = mHead;
        oneBefore = mHead;
        for (i = 0; i < index; i++)
        {
            oneBefore = tmp;
            tmp = tmp->mNext;
        }

        data = tmp->mData;

        if (index == 0)
        {
            mHead = tmp->mNext;

            if (mHead == NULL)
                mTail = NULL;
        }
        else if (index == mCount - 1)
        {
            mTail = oneBefore;
            mTail->mNext = NULL;
        }
        else
            oneBefore->mNext = tmp->mNext;

        tmp->mNext = NULL;
        delete tmp;

        mCount--;
    }

    return data;
}


/*      Pre:  The list is instantiated and the index is valid
 *     Post:  The data in the specified index is returned to the
 *            caller
 *  Purpose:  To retrieve the specified nodes in the list using []
 *            operator
 ************************************************************************/
template <typename T>
T DoublyLinkedList<T>::operator[](int index)
{
    return getData(index);
}

#endif

