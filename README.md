# DSA

## TODO:
## Data Structures:
- Doubly Linked List
- Queue 
- Stack
- Binary Search Tree
- Self Balancing BST
- AVL Tree
- Graph

## Algorithms:
### Searching:
- Linear Search
- Binary Search
- BFS
- DFS
- Dijkstra
- A*
### Sorting:
- Bubble Sort
- Shell Sort
- Merge Sort
- Insertion Sort
- Selection Sort
- Quick Sort
- Heap Sort
### Mining:
